# Rust-Bert-Bot

An Imp Cwtch bot demo using [rust-bert](https://github.com/guillaume-be/rust-bert) to chat

## Usage

Create a `user_config.json` with an allow list of bot peer admins with their cwtch addresses:

Example:

```
{
	"allow_list": ["xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", "yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy"]
}
```

![Rust-Bert bot in Cwtch](rust-bert.png)
